#!/bin/bash
#PBS -N scale
#PBS -q copperhead
#PBS -l nodes=1:ppn=1
#PBS -l mem=16gb
#PBS -l walltime=10:00:00

# Make scaled coverage graph files

# Flaw: this data set was paired end, and many pairs overlap. This means
# positions where pairs overlap have too many counts. Alignments seeming
# to detect rare variants may exhibit bias toward reads with missing mates.

cd $PBS_O_WORKDIR

# S, F passed in from qsub -v option

module load samtools #tabix, bgzip

MOD=100000000
OUT=$S.scaled.bedgraph.gz
if [ ! -s $OUT ]; 
then
    if [ -s $S.count.txt ];
    then 
	N=`cat $S.count.txt`
    else
	N=`samtools view -c $F`
    fi
    scale=`echo "scale=2; $MOD/$N" | bc`
    genomeCoverageBed -ibam $F -split -bg -scale $scale | bgzip > $OUT
    tabix -s 1 -b 2 -e 3 -f -0 $OUT
fi
