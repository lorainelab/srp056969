#!/bin/bash
#PBS -N FJ
#PBS -q copperhead
#PBS -l nodes=1:ppn=1
#PBS -l mem=16gb
#PBS -l walltime=10:00:00

# Make bed files with junctions for spliced alignments
# Score in bed file is the number of spliced read alignments
# that support the junction.


cd $PBS_O_WORKDIR

# -u - use singly-mapped reads only
# -f - require this many bases on flanking exon
# assumes bam files in the same directory
J=find-junctions-1.0.0-jar-with-dependencies.jar
T=H_sapiens_Dec_2013.2bit

# S, F passed in from qsub -v option

# Java8 or higher must be in PATH

module load samtools

if [ ! -s $S.FJ.bed.gz ]; 
then
    java -Xmx32g -jar $J -u -f 5 -b $T -o $S.FJ.bed $F
    if [ -s "$S.FJ.bed" ];
    then
	sort -k1,1 -k2,2n $S.FJ.bed | bgzip > $S.FJ.bed.gz
	rm $S.FJ.bed
	tabix -s 1 -b 2 -e 3 -f -0 $S.FJ.bed.gz
    fi
fi
