#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=1
#PBS -l mem=8gb
#PBS -l walltime=6:00:00

cd $PBS_O_WORKDIR
module load sra-tools

# F defined by qsub -v 
S=`basename $F .sra`
fastq-dump --split-files $F
FS=`ls $S*.fastq`
for F in $FS
do
    gzip $F
done
