#!/bin/env bash

# to run: 
#    qsub-doIt.sh [extension] [script] >jobs.out 2>jobs.err
# to kill:
#    cat jobs.out | xargs qdel 
#
# example)
#
#   qsub-doIt.sh _RNA_1.fastq.gz tophat.sh >jobs.out 2>jobs.err
#

EXT=$1
SCRIPT=$2

if [[ -z "$EXT" ]]; then
    echo "Must provide input file name suffix" 1>&2
    echo "Example: _1.fastq.gz tophat.sh" 1>&2
    exit 1
fi
if [[ -z "$SCRIPT" ]]; then
    echo "Must provide name of script to process input files" 1>&2
    echo "Example: _1.fastq.gz tophat.sh" 1>&2
    exit 1
fi

FS=$(ls *${EXT})
for F in $FS
do
    S=$(basename $F ${EXT})
    CMD="qsub -N $S -o $S.out -e $S.err -vS=$S,F=$F $SCRIPT"
    #echo "running: $CMD"
    $CMD
done
