#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=1
#PBS -l mem=32gb
#PBS -l walltime=16:00:00
cd $PBS_O_WORKDIR
module load bowtie2
bowtie2-build H_sapiens_Dec_2013.fa H_sapiens_Dec_2013


