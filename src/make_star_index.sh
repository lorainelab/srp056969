#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=6
#PBS -l mem=64gb
#PBS -l walltime=16:00:00
cd $PBS_O_WORKDIR
module load star
STAR --runThreadN 6 --runMode genomeGenerate --genomeDir . --genomeFastaFiles H_sapiens_Dec_2013.fa --sjdbGTFfile H_sapiens_Dec_2013.gtf --sjdbOverhang 99
