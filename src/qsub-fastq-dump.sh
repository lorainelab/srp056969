#!/bin/bash

# to run: 
#    qsub-fastq-dump.sh >jobs.out 2>jobs.err
# to kill:
#    cat jobs.out | xargs qdel 

SCRIPT=fastq-dump.sh
FS=`ls *.sra`
for F in $FS
do
    S=`basename $F .sra`
    CMD="qsub -N $S -o $S.out -e $S.err -vF=$F $SCRIPT"
    $CMD
done
