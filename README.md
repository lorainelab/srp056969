## About

This repository contains scripts and analysis results for SRA dataset SRP056969, RNA-Seq data from multiple human tissues.

## Questions?

Contact Ann loraine aloraine@uncc.edu